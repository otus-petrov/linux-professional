# systemd
PIDFile= - нужен для форкающихся процессов
yum list installed systemd - узнать версию
### списки юнитов:
- systemctl --help | grep list
- systemctl --all выводит все юниты
- systemctl list-unit-file
- systemctl list-timers (см. man)
- systemctl --failed - зафейлинные юниты
- systemctl mask/unmask unit.service - запретить для запуска и разрешить для запуска соотвественно
- systemctl cat/show/edit --full - вывести описание юнита/показать параметры/отредактировать юнит 
- systemd-delta - чем юниты расширены(конфигами) или перенаправлено
- man systemd.index
- .slice -  юнит группировки
- systemd-cgls - показывает листинг контрольных групп в виде дерева
- systemctl cat network.target - так можно просмотреть таргет. Таргет использутеся для групировки процессов
    - AllowIsolate=yes - показывает, можно ли  этот юнит загружаться, если этой опции нет, то в этот юнит нельзя загружаться(используется только для группировки других сервисов, сокетов, монтирования и тд, для параллельной загрузки)
1. systemd-analyze [time] - посмотреть сколько времени загружалось kernel, initrd, userspace
2. systemd-analyze critical-chain - посмотреть что загружалось дольше всего
3. systemd-analyze blame - вывод без последовательности(цепочки) по каждому юниту 
- systemctl list-dependencies - отображает дерево, в котором происходит загрузка
    - красным отражены юниты, условия для запуска которых не было выполнено
    - зеленым - юниты, условие для запуска которых выполнено, но не факт что они запущены
    - systemctl --failed - зафейлинные юниты
### обязательные секции юнита
- [unit]
- [тип юнита]
- [install]
- вторая секция соотвествует типу юнита(то что после точки)

## что включает в себя systemd
- Утилиты systemd
- Демоны systemd
- Таргеты systemd
- Базовые компоненты
- Библиотеки systemd
- Компоненты ядра linux

## cgroups
- управление
    - определение принадлежности процесса сервису
    - systemctl status sshd.servise - там есть пид
- ограничение
    - ограничение ресурсов
- аудит
    - иная перспектива обзора
### Основные определения:
- cgroup - контрольная группа (сервис / сессия )
- slice - объект, представляющий иерархию (сервиса / сессии)
- scope - объект в slice, группирующий процессы

- systemd-cgtop - посмотреть slice
    - ls -l /sys/fs/cgroup/systemd/ - там мы можем увидеть наш user.slice
        - в нем мы можем увидеть сессии и в каждой - свои определения контрольных групп
    - по умолчанию мы видим только корень
    - настройки в /etc/systemd/system.conf, после смены настроек systemctl daemon-reexec - для переинициализации
        - там есть параметры в имени с "Accounting" - это учёт
    - systemctl-cgtop --depth - ограничивает уровень грубины от корня
- ps -afxo pid,user,comm,cgroup

## Блок 2. unit
### типы юнитов
- target - ничего не описывает, группирует другие юниты
- service - аналог демона (или то, что можно запустить)
- timer - аналог cron (запуск другого юнита, default - *.service)
- device - факт подключения устройства (sysfs-имя устройства)
- mount - точка монтирования файловой системы
- automount - точка автомонтирования (*.mount)
- socket - запуск юнита при подключении к указанному сокету (default - *.service)
- path - запуск юнита по событию доступа к пути (default - *.service)
- slice - группирует другие юниты в дереве cgroups
- swap - управление swap’ом (*.device)
- snapshot - снимки состояния сервисов
- scope - “области” или “границы”, заданные systemd
### Основные каталоги
- Системные
    - /usr/lib/systemd/ - основной каталог
    - /usr/lib/systemd/system/ - юнит-файлы пакетов
    - /usr/lib/systemd/system/*.wants/ - симлинки предопределенные systemd
- Админские
    - /etc/systemd/ - конфигурация
    - /etc/systemd/system - юнит файлы администратора *
    - /etc/systemd/system/*.wants/ - симлинки при systemctl enable
    - /etc/sysconfig/ - переменные

- EnvironmentFile= - указываем директорию, где брать переменные для этого сервиса
- Wants= - здесь указываем не обязательное требования для запуска (имя сервиса)
- Requarements= - тут обазятельные требования, без этого условия сервис не стартанет

## Ulimits
- man ulimit || man limits.conf
- ulimit - ограничение ресурсов на процессы (для каждого процеса )
    - cat /etc/security/limits.conf
    - если создать ограничение на количество файлов создаваемых процессом - то процесс может даже отказать в соединении, из-за того что не сможет создать файл для этого соединения
    - ulimit -a текущие лимиты можно посмотреть
    - лимиты устаналиваются на юнит, но дочерние процессы пользуются общий лимит
### systemcd limits
- service,mount,etc
    - Limit
- man systemd.exec (/Limit) - почитать какие лимиты есть 
- /etc/systemd/system.conf здесь можно указать лимиты для юнитов
- /etc/systemd/system.conf здесь можно указать лимиты для пользователей