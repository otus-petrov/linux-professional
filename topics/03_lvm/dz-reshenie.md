- pvcreate /dev/sdb
- vgcreate vg_root /dev/sdb
- lvcreate -n lv_root -l +100%FREE /dev/vg_root
- mkfs.xfs /dev/vg_root/lv_root
- mount /dev/vg_root/lv_root /mnt
- xfsdump -J - /dev/VolGroup00/LogVol00 | xfsrestore -J - /mnt - этой командой копируем все данные с / раздела в /mnt
- for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done - перемонтируем все каталоги в mnt
- chroot /mnt/ - назначаем корневый каталог в /mnt
- grub2-mkconfig -o /boot/grub2/grub.cfg - создать конфиг
- cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done - Обновим образ initrd
- чтобы при загрузке был смонтирован нужны root нужно в файле /boot/grub2/grub.cfg заменить rd.lvm.lv=VolGroup00/LogVol00 на rd.lvm.lv=vg_root/lv_root
- lvremove /dev/VolGroup00/LogVol00 - удаляем LV
- lvcreate -n VolGroup00/LogVol00 -L 8G /dev/VolGroup00 - создаем такой же, только меньше
- mkfs.xfs /dev/VolGroup00/LogVol00 - создаем ФС
- mount /dev/VolGroup00/LogVol00 /mnt - понтиируем в /mnt
- xfsdump -J - /dev/vg_root/lv_root | xfsrestore -J - /mnt - копируем данные с раздела / на /mnt
- for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done - перемонтируем все каталоги в mnt
- chroot /mnt/ - назначаем корневый каталог в /mnt
- grub2-mkconfig -o /boot/grub2/grub.cfg - создать конфиг
- cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done - Обновим образ initrd  

Пока не перезагружаемся и не выходим из под chroot - мы можем заодно перенести /var
- На свободных дисках создаем зеркало:
    - pvcreate /dev/sdc /dev/sdd
    - vgcreate vg_var /dev/sdc /dev/sdd
    - lvcreate -L 950M -m1 -n lv_var vg_var
- mkfs.ext4 /dev/vg_var/lv_var - создаем на нем ФС
- перемещаем туда /var
    - mount /dev/vg_var/lv_var /mnt
    - cp -aR /var/* /mnt/
    - rsync -avHPSAX /var/ /mnt/
- mkdir /tmp/oldvar && mv /var/* /tmp/oldvar - сохраняем содержимое старого var
- umount /mnt - отмонтируем
- mount /dev/vg_var/lv_var /var - монтируем в /var
- echo "`blkid | grep var: | awk '{print $2}'` /var ext4 defaults 0 0" >> /etc/fstab - Правим fstab для автоматического монтирования /var
- перезагружаемся в новый уменьшенный root  и удаляем временную VG
    - lvremove /dev/vg_root/lv_root
    - vgremove /dev/vg_root
    - pvremove /dev/sdb  

Выделим том под home:
- lvcreate -n LogVol_Home -L 2G /dev/VolGroup00
- mkfs.xfs /dev/VolGroup00/LogVol_Home
- mount /dev/VolGroup00/LogVol_Home /mnt/
- cp -aR /home/* /mnt/
- rm -rf /home/*
- umount /mnt
- mount /dev/VolGroup00/LogVol_Home /home/
- echo "`blkid | grep Home | awk '{print $2}'` /home xfs defaults 0 0" >> /etc/fstab  

Сделать том для снапшотов
- touch /home/file{1..20} - генерируем файлы в /home/
- lvcreate -L 100MB -s -n home_snap /dev/VolGroup00/LogVol_Home - снимаем снапшот
- rm -f /home/file{11..20} - 3даляем часть файлов
- umount /home
- lvconvert --merge /dev/VolGroup00/home_snap - возвращаемся к снепшоту
- mount /home