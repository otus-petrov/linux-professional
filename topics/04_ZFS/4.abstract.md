# ZFS  

## Компоненты ZFS
- SPA(Storage Pool Allocator)
- DMU(Data Management Unit)
- ZFS POSIX Layer
- ZFS Volume Emulator

## SPA Storage Pool Allocator  
Пул устройств хранения данных:
- описывает физические характеристики хранения
    - размещение устройств
    - избыточность данных
- выступает в качестве хранилища данных

## DMU Data management unit
- предоставляет транзакционную модель поверх SPA
    - транзация это серия действий которые записываются на диск группой
- клиенты DMU работают с объектами, наборами объектов, транзакциями
    - ZFS POSIX Layer
    - ZFS Volume Emulator

Для создания и монтирования диска нужно 2 команды:
- zpool create tank mirror sde sdf mirror sdg sdh - создать пул дисков, в зеркале 2+2
- zfs create tank/test2 - создать файловую систему zts с названием test2 в пуле tank и смонтировать

## Целостность данных (data integrity)
- CoW copy-on-write
    - никогда не перезаписывает данные (записываются только изменения)
    - состояния на диске всегда корректные
    - следствие: нет fsck
- транзакционность (DMU)
    - связанные действия обрабатываются целиком
    - следствие: не нужен журнал
- Контролные суммы (checksums, Mercle tree)
- избыточность данных
    - raidz
    - ditto blocks. metadata copy (2 default. 1 per disk)

## Кеширование чтения
- Adaptive Replacement Cache (ARC)
    - кэш в памяти ( занимает до 50% от доступной RAM)
- Layer-2 Adaptive Replacement Cache » (L2ARC)
    - кэш на диске (быстрые диски SSDs или SAS 15k)
    - гибридный кэш большого размера
- Кэширует то что недавно читалось и часто читается
    - увеличение производительности чтения

## Кеширование записи
- ZFS Intent Log (ZIL)
    - журнал записей
    - обеспечивает целостность данных
- Separate Intent Log » (SLOG)
    - рекомендуется размещать на быстрых дисках SSDs или SAS 15k
    - используется только для синхронных операций записи
    - увеличивает скорость записи

## Практика

### zpool

- zpool create poolmirror sdb sdc - создать пул из двух дисков с названием poolmirror
- zpool list - список
- zpool status - подробная инфа о пулах
- zpool destroy pollmirror - удалить без вопросов
- echo disk{1..6} | xargs -n 1 fallocate -l 500M - создать 6 файлов по 500мб
- zpool create stripe $PWD/disk[1-5] - создать пулл, аналог рейда 0
- zpool create mymirror mirror $PWD/disk[1-5] - создать пулл, аналог рейда 1
- zpool create raid raidz1 $PWD/disk[1-3] - собрать рейд.
    - z1 - коливество дисков, кторое может выйти из строя без потери данных. и так до 3х
    - чем выше рейд, тем медленнее 
- zpool create hybrid mirror $PWD/disk[12] mirror $PWD/disk[34] - 2 зеркала, объединенных страйпингом(рейд0) аналог рейда 10
- zpool create hybrid2 raidz2 $PWD/disk[1-3] raidz2 $PWD/disk[4-6] - тоже гибрид, только с рейдзет. Аналог рейда 6.0
- Создание pool с SLOG и L2LARC. добавить диски для кеша и лога можно:
    - сразу:
        - zpool create storage mirror sdb sdc log mirror nvme0n1 nvme0n2 cache nvme0n3 nvme0n4
    - или позже:
        - zpool create storage mirror sdb sdc
        - zpool add storage cache nvme0n3 nvme0n4 # L2ARC
        - zpool add storage log mirror nvme0n1 nvme0n2 # SLOG


### файловая система
- zfs create storage/data - создать файловую систему
- zfs create storage/data/video - файловая система может быть вложенной
- zfs list - посмотреть файловые системы
- zfs get mounted - посмотреть что замонтированно
- zfs get mountpoint - что и куда замонтированно
- zfs set mountpoint=/home/testuser storage/userdir - смонтировать storage/userdir в /home/testuser
- dd if=/dev/urandom of=/home/testuser/file bs=1M - забить  полностью пул райндомными данными
- zfs get quota - посмотреть квоты на разделы
- zfs set quota=500M storage/userdir - установить квоту для раздела
- zfs set reservation=200M storage/data/video - зарезервировать 200М для файловой системы
- Параметры размера блока:
    - zfs get recordsize - посмотреть параметры про размер блока файловой системы
    - zfs set recordsize=1M storage/data/movies - установить размер блока
    - zfs set checksum=sha256 storage/data/movies - изменить размер контрольной суммы
    - zfs get checksum - посмотреть контрольные суммы на всех файловых системах
- zfs get all storage/data/movies - посмотреть все параметры файловой системы

#### сжатие и дедупликация:
- ZFS может сжимать данные при записи на файловую систему и разжимать их при чтении только новые данные сжимаются. Старые не перепаковываются
- zfs set compression=on storage/src/compressed - включить сжатие в фс
- zfs get compression,compressratio storage/src/compressed - проверить статус сжатия
- дедупликация - система видит обинаковые блоки на 2х дисках и размещает по половине на каждом. так сокращается место на диске, но это жрет память, так как система держит информацию о файлах в памяти, чтобы сравнивать с новой.
    - zfs get dedup storage/src/dedup - посмотреть информацию о дедупликации
    - zfs set dedup=on storage/src/dedup - включить дедупликацию

### Перенос дисков между хостами
- zpool export storage - подготовить диски к экспорту
- перенести диски
- zpool import -d ${PWD} - посмотреть какие пулы доступны из текущей папки для импорта
- zpool import -d ${PWD} storage - импортировать пул storage в систему из файлов в текущей папке. вместе с файловыми системами.

### snapshot
- zfs snapshot storage/data/music@snap001 - Создать снимок
- zfs list -t snapshot - Список снепшотов
- zfs destroy storage/data/music@snap001 - удалить снепшот
- zfs rollback storage/data/music@snap001 - восстановить файлы из снимка

#### Пример переноса snapshots между хостами
- Хост номер 1
    - zfs send storage/text@copy001 > snapshot
- Хост номер 2
    - zfs receive storage/data/text2 < snapshot
    - zfs list -t snapshot
    - zfs rollback storage/text2@copy001
    - zfs mount otus

- dd if=/dev/zero of=/dev/sdb - убить диск
- zpool scrub poolmirror - проверить пул
- zpool replace -f poolmirror sdb sdd - заменить
