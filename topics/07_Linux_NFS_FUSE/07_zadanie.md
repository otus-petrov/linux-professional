- командой vagrant up поднимается клиент и сервер
- на сервере расшарена директория, которая примаунчена на клиенте
- в расшаренной директории есть папка upload с правами на запись
- монтирование выполняется с помощью systemd (automount)
- nfs3 со включенным udp при включенномм файерволе

server
yum install nfs-utils -y
mkdir -p /var/nfs_share/upload
chmod 777 /var/nfs_share/upload/
echo -e "/var/nfs_share 192.168.50.11(ro,root_squash,no_all_squash)\n/var/nfs_share/upload 192.168.50.11(rw,root_squash,no_all_squash)" > /etc/exports
systemctl start rpcbind
systemctl start nfs-server.service
exportfs -r

client
sudo yum install nfs-utils
sudo systemctl start rpcbind
sudo systemctl enable rpcbind
sudo mkdir /mnt/nfs_share
sudo mount -t nfs 192.168.50.10:/var/nfs_share/ /mnt/nfs_share
sudo mount -t nfs 192.168.50.10:/var/nfs_share/upload /mnt/nfs_share/upload
showmount --all 192.168.50.10

